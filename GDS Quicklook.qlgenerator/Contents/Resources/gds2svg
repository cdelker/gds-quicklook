#!/usr/bin/python
''' Convert GDS-II to SVG/ASCII '''

#-----------------------------------------------------------
# Copyright (c) 2014 Collin J. Delker, www.collindelker.com
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer. 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution. 
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#-----------------------------------------------------------

import sys
from math import sin, cos, radians

#---------------------------------------------------------------------
# Define some constants
#---------------------------------------------------------------------
record_types = ['HEADER', 'BGNLIB', 'LIBNAME', 'UNITS', 'ENDLIB', 'BGNSTR', 'STRNAME', 'ENDSTR',
    'BOUNDARY', 'PATH', 'SREF', 'AREF', 'TEXT', 'LAYER', 'DATATYPE', 'WIDTH', 'XY', 'ENDEL',
    'SNAME', 'COLROW', 'TEXTNODE', 'NODE', 'TEXTTYPE', 'PRESENTATION', 'SPACING', 'STRING',
    'STRANS', 'MAG', 'ANGLE' ]

imgwidth  = 770   # Width/height of SVG in pixels
imgheight = 570

huelist = [ ((x*80)+5)%360 for x in range(9) ]   # Color table hue/lightness
lgtlist = [40,50,60,70,80,90,100]

#---------------------------------------------------------------------
# Helper functions
#---------------------------------------------------------------------
def get_window_size( element_list ):
    ''' Get the size of the entire drawing '''
    xmin =  float("inf")
    xmax = -float("inf")
    ymin =  float("inf")
    ymax = -float("inf")

    for e in element_list:
        if e==None: continue
        for XY in e.XY:
            if XY[0] > xmax: xmax = XY[0]
            if XY[0] < xmin: xmin = XY[0]
            if XY[1] > ymax: ymax = XY[1]
            if XY[1] < ymin: ymin = XY[1]

    return xmin, ymin, xmax-xmin, ymax-ymin


def print_html_hdr( xmin, ymin, xlen, ylen ):
    ''' Write the HTML/SVG header to stdout. '''
    print '<!DOCTYPE html>'
    print '<html>'
    print '<body>'
    print '<svg width="%dpx" height="%dpx" viewBox="%f %f %f %f">' % (imgwidth, imgheight, xmin, ymin, xlen, ylen )


def get_color_for_layer( layer ):
    ''' Select a color for the given drawing layer. '''

    if layer == None: layer = 1
    layer = layer % (len(huelist)*len(lgtlist))

    h = huelist[ layer % len(huelist) ]
    l = lgtlist[ (layer / len(lgtlist))%len(lgtlist) ]  # integer divide
    s = 70
    return 'hsl(%d, %d%%, %d%%)'%(h,s,l)


def bytearray_to_int( data, signed=True ):
    ''' Convert byte data to integer. '''
    val = 0
    data = data[::-1]
    bits = len(data) * 8

    for i in range(len(data)):
        val = val + ( data[i] << (i*8) )

    if signed:       # Two's complement
        if( (val & (1<<(bits-1))) != 0 ):
                val = val - (1<<bits)
    return val


def bytearray_to_float( data ):
    ''' Convert byte data to float (using GDS defined representation). '''
    bits = (len(data)-1) * 8

    sign = (data[0] & 0x80) / 0x80
    exp = (data[0] & 0x7F) - 64                 # Excess-64 representation
    mantissa = 0

    for i, byte in enumerate( data[1:][::-1] ):
        mantissa = mantissa + (byte<<(i*8))
    mantissa = float(mantissa) / (2**bits)      # MSB = 1/2

    val = mantissa * 16**exp
    if sign:
        val = -val
    return val


def bytearray_to_str( data ):
    ''' Convert byte data to string. GDS always uses even-length strings! '''
    if data[-1] == 0:
        return str(recdata[:-1])
    else:
        return str(recdata)


def rotate_points( XY_list, theta ):
    ''' Rotate the XY coordinates by angle theta. '''
    costheta = cos(theta)
    sintheta = sin(theta)
    return [ (XY[0]*costheta - XY[1]*sintheta, XY[0]*sintheta + XY[1]*costheta) for XY in XY_list ]


def ofst_points( XY_list, ofst ):
    ''' Translate XY coordinates by ofset '''
    return [ (XY[0]+ofst[0], XY[1]+ofst[1]) for XY in XY_list ]


def reflectX( XY_list ):
    return [ (XY[0], -XY[1]) for XY in XY_list ]


#--------------------------------------------------------------------
# Classes to hold data
#--------------------------------------------------------------------
class svg_element():
    ''' Class to hold each SVG element data '''
    def __init__( self, XY, color=(255,0,0), string=None, fill=True, lw=1 ):
        self.XY = XY
        self.string = string
        self.fill = fill
        self.color = color
        self.lw = lw
        
    def write_SVG( self, lw_min=1 ):
        ''' Write the element SVG tags to stdout '''

        if self.string != None:   # Write text element
            size=lw_min*10   # Make approx 10 point font size.
            print '<text x="%f" y="%f" fill="%s" font-size="%f">' % (self.XY[0][0], self.XY[0][1], self.color, size),
            print '%s</text>' % (self.string)
                
        else:  # Write boundary/path element
            # Determine fill and line width
            if self.fill:   # Boundary
                fill = '%s'%(self.color)
                lw = 1
            else:           # Path
                fill = 'none'

                # Ensure minimum line width
                if self.lw > lw_min:
                    lw = self.lw
                else:
                    lw = lw_min

            print '<path d="M%f %f ' % (self.XY[0][0], self.XY[0][1]),            
            for p in self.XY[1:]:
                print 'L%f %f ' % (p[0], p[1]),
            print '" stroke="%s" stroke-width="%f"' % (self.color, lw),
            print 'fill="%s" />'%fill


class element():
    ''' Element of GDS file. Path, Text, Structure-Reference, etc. '''
    def __init__(self, type):
        ''' Set up empty element '''
        self.XY = []
        self.layer = None
        self.type = type   # Boundary, path, struc, etc.
        self.string = ''
        self.angle = 0
        self.sref = None
        self.lw = 0
        self.reflectX = False

    def add_XY(self, XY):
        ''' Add an XY coordinate to list. '''
        self.XY.append( XY )

    def build_svg( self, ofstlist=[], anglelist=[], reflist=[] ):
        ''' Draw the element by writing SVG to stdout '''

        svglist = []

        XY = self.XY
        if self.type != 'AREF':
            # Rotations must be done in reverse order
            for ofst, angle, refX in zip(ofstlist[::-1], anglelist[::-1], reflist[::-1] ):
                if refX > 0:
                    XY = reflectX( XY )
                XY = rotate_points( XY, angle )
                XY = ofst_points( XY, ofst )

        color = get_color_for_layer(self.layer)
        if self.type == 'BOUNDARY' or self.type == 'PATH':
            svglist.append( svg_element( XY, color=color, fill=(self.type=='BOUNDARY'), lw=self.lw ) )

        elif self.type == 'TEXT':
            svglist.append( svg_element( XY, color=color, string=self.string ) )

        elif self.type == 'SREF' and self.sref is not None:
            svglist.extend( self.sref.build_svg( ofstlist=ofstlist+self.XY, anglelist=anglelist+[self.angle], reflist=reflist+[self.reflectX] ) )
            
        elif self.type == 'AREF' and self.sref is not None:
            center = XY[0]  # Center of first array element

            colspc = ( (XY[1][0]-XY[0][0]) / self.colrow[0], (XY[1][1]-XY[0][1]) / self.colrow[0] )
            rowspc = ( (XY[2][0]-XY[0][0]) / self.colrow[1], (XY[2][1]-XY[0][1]) / self.colrow[1] )

            for r in range(self.colrow[1]):
                for c in range(self.colrow[0]):
                    ofstX = center[0] + r*rowspc[0] + c*colspc[0]
                    ofstY = center[1] + r*rowspc[1] + c*colspc[1]
                    ofst = (ofstX, ofstY)
                    svglist.extend( self.sref.build_svg( ofstlist=ofstlist+[ofst], anglelist=anglelist+[self.angle], reflist=reflist+[self.reflectX] ) )
        else:
            pass

        return svglist
        

class structure():
    ''' GDS Structure. Contains list of elements. '''
    def __init__(self):
        self.name = None
        self.element_list = []
    
    def add_element(self, element):
        ''' Add element to structure '''
        self.element_list.append( element )
    
    def build_svg( self, ofstlist=[], anglelist=[], reflist=[] ):
        ''' Draw all elements in the structure '''
        # Loop through elements and print SVG for each one.
        svglist = []
        for e in self.element_list:
            svglist.extend( e.build_svg( ofstlist=ofstlist, anglelist=anglelist, reflist=reflist ) )
        return svglist


#----------------------------------------------------------------------
# Main program entry
#----------------------------------------------------------------------
if __name__ == '__main__':

    # Read the file into data stream
    if len(sys.argv[1:])<1:
        quit()

    f = open(sys.argv[1], 'rb')
    stream = bytearray(f.read())

    cur_structure = None
    cur_element = None

    structure_list = []

    # Loop through each structure
    i=0
    while i < len(stream):
        length = (stream[i+0] << 8) + (stream[i+1])
        rectype = stream[i+2]
        dattype = stream[i+3]
        recdata = stream[i+4:i+length]

        if length == 0:
            length = 4      # 4-byte minimum record size!

        if rectype >= len(record_types):
            # Unsupported record type. Skip it.
            i = i + length
            continue

        recname = record_types[rectype]

        # Structure properties
        if recname == 'BGNSTR':
            cur_structure = structure()

        elif recname == 'STRNAME':
            cur_structure.name = bytearray_to_str(recdata)
    
        elif recname == 'ENDLIB':
            break
    
        # Standard elements
        elif recname in ['BOUNDARY', 'PATH', 'SREF', 'AREF', 'TEXT']:
            cur_element = element( recname )

        # Element properties
        elif recname == 'LAYER' and cur_element is not None:
            cur_element.layer = bytearray_to_int(recdata)
    
        elif recname == 'STRING' and cur_element is not None:
            cur_element.string = bytearray_to_str(recdata)

        elif recname == 'XY' and cur_element is not None:
            for coord in range(len(recdata)/8):     # Each X or Y is 4 bytes, read both at once.
                x = bytearray_to_int( recdata[coord*8:coord*8+4] )
                y = -bytearray_to_int( recdata[coord*8+4:coord*8+8] )   # Y-coordinates are "upside down".
                cur_element.add_XY( (x,y) )

        elif recname == 'WIDTH' and cur_element is not None:
            cur_element.lw = bytearray_to_int( recdata )

        elif recname == 'SNAME':    # Structure reference name
            cur_element.sname = bytearray_to_str(recdata)

        elif recname == 'ANGLE':    # Only used in structure references
            cur_element.angle = -radians( bytearray_to_float( recdata ) )

        elif recname == 'COLROW':   # Defines row/column number in AREF structure array.
            cur_element.colrow = (bytearray_to_int(recdata[0:2]), bytearray_to_int(recdata[2:4]))

        elif recname == 'STRANS':
            cur_element.reflectX = (bytearray_to_int(recdata) & 0x8000)

        # Closing records
        elif recname == 'ENDEL' and cur_element is not None:
            cur_structure.add_element( cur_element )
            cur_element = None

        elif recname == 'ENDSTR':
            structure_list.append( cur_structure )
            cur_structure = None

        else:
            # Record unused. Some unsupported ones are: STRANS (reflections), MAGnification, etc.
            pass

        i = i + length
    f.close()

    if len(structure_list) == 0:
        sys.stderr.write( 'No structures defined!\n' )
        quit()  # No structures!

    # Link up structure references
    for struc in structure_list:
        for e in struc.element_list:
            if e.type == 'SREF' or e.type == 'AREF':
                for s in structure_list:
                    if e.sname == s.name:
                        e.sref = s
                        break

    # Try to determine top-level structure
    referenced_strucs = []
    for struc in structure_list:
        referenced_strucs.extend( [x.sname for x in struc.element_list if (x.type == 'SREF' or x.type == 'AREF') ] )

    top_level = [x for x in structure_list if x.name not in referenced_strucs]
    display_struc = top_level[-1]

    if len(top_level) > 1:
        sys.stderr.write( 'Multiple top-level structures. Using last.' )
    elif len(top_level) == 0 and len(structure_list)>0:
        sys.stderr.write( 'No top-level structures. Using last defined.' )
        display_struc = structure_list[-1]

    # Build list of SVG components
    svg_elements = display_struc.build_svg()
    xmin, ymin, xlen, ylen = get_window_size( svg_elements )

    # Adjust aspect ratio to be equal
    if xlen < ylen:
        xlennew = ylen / imgheight * imgwidth
        if xlen < xlennew:
            xmin = xmin - (xlennew-xlen)/2
            xlen = xlennew
    elif xlen > ylen:
        ylennew = xlen / imgwidth * imgheight
        if ylen < ylennew:
            ymin = ymin - (ylennew-ylen)/2    
            ylen = ylennew

    # Ensure minimum linewidth is at least one pixel
    lw_min = float(xlen) / imgwidth * 2

    # Write it all out to stdout
    print_html_hdr( xmin, ymin, xlen, ylen )
    for e in svg_elements:
        if e==None: continue
        e.write_SVG( lw_min=lw_min )

    print '</svg>\n</body>\n</html>\n'
