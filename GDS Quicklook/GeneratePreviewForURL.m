#import <CoreFoundation/CoreFoundation.h>
#import <CoreServices/CoreServices.h>
#import <QuickLook/QuickLook.h>
#import <Cocoa/Cocoa.h>

/* -----------------------------------------------------------------------------
 Generate a preview for file
 
 This function's job is to create preview for designated file
 ----------------------------------------------------------------------------- */

NSData* processGDS(NSURL* url);

OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options)
{
    
    CFDataRef previewData;
    
    previewData = (CFDataRef) processGDS((NSURL*) url);
    
    if (previewData) {
        
        CFDictionaryRef properties = (CFDictionaryRef) [NSDictionary dictionary];
        QLPreviewRequestSetDataRepresentation(preview, previewData, kUTTypeHTML, properties);
    }
    
    return noErr;
}


NSData* processGDS(NSURL* url)
{
    
    NSString *path2GDS = [[NSBundle bundleWithIdentifier:@"net.delksterGDS.quicklook"] pathForResource:@"gds2svg" ofType:nil];
    
	NSTask* task = [[NSTask alloc] init];
	[task setLaunchPath: [path2GDS stringByExpandingTildeInPath]];
	
	[task setArguments: [NSArray arrayWithObjects: [url path], nil]];

	NSPipe *readPipe = [NSPipe pipe];
	[task setStandardOutput:readPipe];
	
	[task launch];
    
	
	NSData *gdsData = [[readPipe fileHandleForReading] readDataToEndOfFile];
    
    [task release];
	return gdsData;
}

void CancelPreviewGeneration(void* thisInterface, QLPreviewRequestRef preview)
{
    // implement only if supported
}
